<?php
/**
 * @file
 * Settings administration for forumautodelete.module.
 */

/**
 * Define settings form.
 */
function forumautodelete_settings() {
  $forums = forum_get_forums();

  if (count($forums)) {
    foreach ($forums as $tid => $forum) {
      if (!$forum->container) {
        $form['forumautodelete_'. $tid] = array(
          '#type' => 'textfield',
          '#title' => t('Delete @name posts after', array('@name' => $forum->name)),
          '#description' => t('Use <a href="!strtotime_url">strtotime</a> format, e.g. -2 months. Leave blank to not delete posts.', array(
            '!strtotime_url' => url('http://php.net/manual/en/function.strtotime.php'),
          )),
          '#default_value' => variable_get('forumautodelete_'. $tid, ''),
        );
      }
    }
  }
  else {
    $form['null'] = array(
      '#value' => t('No forums detected.'),
    );
  }
  return system_settings_form($form);
}